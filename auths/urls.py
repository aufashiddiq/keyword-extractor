from django.urls import path

from . import views

app_name = 'auths'
urlpatterns = [
    path('', views.index_view, name='index'),
    path('login/', views.login_view, name='login'),
    path('register/', views.register_view, name='register'),
    path('logout/', views.logout, name='logout'),

    path('home/', views.home_view, name='home'),
    path('article/', views.article_view, name='article'),
    path('article_create/', views.article_create_view, name='article_create'),
    path('article_create_post/', views.article_create_post, name='article_create_post'),
    path('<int:article_id>/article_detail/', views.article_detail_view, name='article_detail'),

    path('login_post/', views.login_post, name='login_post'),
    path('register_post/', views.register_post, name='register_post'),

    path('<int:user_id>/update_view/', views.update_view, name='update_view'),
    path('<int:user_id>/update_post/', views.update_post, name='update_post'),

    path('<int:user_id>/delete/', views.delete, name='delete'),
]