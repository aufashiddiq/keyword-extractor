from django.shortcuts import render
from django.views import View, generic
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse

from .models import User, Article

def index_view(request):
    try:
        sess = request.session['username']
    except(KeyError):
        return render(request, 'auths/index.html')
    else:
        return HttpResponseRedirect(reverse('auths:home'))

def home_view(request):
    try:
        sess = request.session['username']
    except(KeyError):
        return render(request, 'auths/index.html', {
            'error_message': "You haven't been login yet.",
        })
    else:
        user_list = User.objects.all()
        context = {'user_list': user_list}
        return render(request, 'auths/home.html', context)


def login_view(request):
    return render(request,'auths/login.html')

def register_view(request):
    return render(request,'auths/register.html')

def logout(request):
    try:
        del request.session['user_id']
        del request.session['username']
    except KeyError:
        pass
    return HttpResponseRedirect(reverse('auths:index'))

def login_post(request):
    try:
        user_data = User.objects.get(username=request.POST['username'])
    except (KeyError, User.DoesNotExist):
        return render(request, 'auths/login.html', {
            'error_message': "Invalid Username",
        })
    else:
        if user_data.password == request.POST['password']:
            request.session['user_id'] = user_data.id
            request.session['username'] = user_data.username
            return HttpResponseRedirect(reverse('auths:home'))
        else:
            return render(request, 'auths/login.html', {
            'error_message': "Invalid Password.",
        })

def register_post(request):
    try:
        user_data = User(
            fullname=request.POST['fullname'],
            username=request.POST['username'],
            password=request.POST['password']
        )
    except (KeyError, User.DoesNotExist):
        return render(request, 'auths/register.html', {
            'error_message': "You haven't been insert data.",
        })
    else:
        user_data.save()
        request.session['user_id'] = user_data.id
        request.session['username'] = user_data.username
        return HttpResponseRedirect(reverse('auths:home'))

def update_view(request, user_id):
    try:
        user = User.objects.get(pk=user_id)
    except(User.DoesNotExist):
        return render(request, 'auths/update.html', {
            'error_message': "User not available.",
        })
    else:
        return render(request,'auths/update.html', {'user': user})

def update_post(request, user_id):
    try:
        user = User.objects.get(pk=user_id)
        fullname = request.POST['fullname']
        username = request.POST['username']
        password = request.POST['password']

    except (KeyError, User.DoesNotExist):
        return render(request, 'auths/update.html', {
            'error_message': "Update Failed.",
        })
    else:
        user.fullname = fullname
        user.username = username
        user.password = password
        user.save()
        return HttpResponseRedirect(reverse('auths:home'))

def delete(request, user_id):
    try:
        user = User.objects.get(pk=user_id)
    except (User.DoesNotExist):
        return render(request, 'auths/home.html', {
            'error_message': "Delete Failed.",
        })
    else:
        user.delete()
        return HttpResponseRedirect(reverse('auths:home'))

def article_view(request):
    try:
        user_id = request.session['user_id']
        user = User.objects.get(pk=user_id)
    except(KeyError, User.DoesNotExist):
        return render(request, 'auths/index.html', {
            'error_message': "You haven't been login yet.",
        })
    else:
        article_list = user.article_set.all()
        context = {'article_list': article_list}
        return render(request, 'auths/article.html', context)

def article_create_view(request):
    return render(request, 'auths/article_create.html')

def article_create_post(request):
    try:
        user_id = request.session['user_id']
        user = User.objects.get(pk=user_id)
        title = request.POST['title']
        content = request.POST['content']
    except (KeyError, User.DoesNotExist):
        return render(request, 'auths/article_create.html', {
            'error_message': "Failed to create article",
        })
    else:
        user.article_set.create(title=title, content=content)
        return HttpResponseRedirect(reverse('auths:article'))

def article_detail_view(request, article_id):
    try:
        article = Article.objects.get(pk=article_id)
    except(Article.DoesNotExist):
        return render(request, 'auths/home.html', {
            'error_message': "Failed to load article",
        })
    else:
        return render(request,'auths/article_detail.html', {'article': article})