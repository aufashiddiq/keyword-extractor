from django.db import models

class Article(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    original_keyword = models.TextField()
    extracted_keyword = models.TextField()
    accuracy = models.CharField(max_length=200)
    load_time = models.CharField(max_length=20)
    extracted_keyword_ori = models.TextField()
    accuracy_ori = models.CharField(max_length=200)
    load_time_ori = models.CharField(max_length=20)

    def __str__(self):
        return self.title

class Katadasar(models.Model):
    katadasar = models.CharField(max_length=70)
    tipe_katadasar = models.CharField(max_length=25)

    def __str__(self):
        return self.katadasar