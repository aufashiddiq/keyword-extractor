from nltk.tokenize import sent_tokenize, word_tokenize, WordPunctTokenizer
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
import re
import networkx as nx
# import PyPDF2
import tika
tika.initVM()
from tika import parser

from .models import Katadasar

# Tokenize
class Tokenize:

    def sentence_token(text):
        return text.split('. ')

    def word_token(text):
        result = text.lower()
        result = re.sub(r'[^a-z0-9 .,:/\(\)-]', ' ', result, flags = re.IGNORECASE|re.MULTILINE)
        result = re.sub(r'[,:/\(\)]', ' , ', result, flags = re.IGNORECASE|re.MULTILINE)
        result = re.sub(r'[.]', ' . ', result, flags = re.IGNORECASE|re.MULTILINE)
        result = re.sub(r'( +)', ' ', result, flags = re.IGNORECASE|re.MULTILINE)
        return result.strip().split(' ')

# Stemming with Sastrawi Library
def stemming(text):
    factory = StemmerFactory()
    stemmer = factory.create_stemmer()
    return stemmer.stem_token(text)


# Import PDF
class ImportPdf:

    def __init__(self, url):
        parsed = parser.from_file('D:\\College\\Script C\\data\\UNIMED Articles\\'+url)
        self.page_content = parsed["content"]
    
    def get_all_content(self):
        return self.page_content
    
    def get_title(self):
        temp_title = self.page_content[170:400].split(' ')
        title = ''
        for word in temp_title:
            if word.isupper() and len(word) > 1:
                title += word+' '
        return title

    def get_content(self):
        temp_abs = self.page_content[300:3000]
        temp_isi = self.page_content[1000:len(self.page_content)]

        if temp_abs.find("Kata Kunci") > -1:
            abstrak = temp_abs[temp_abs.find("Abstrak"):temp_abs.find("Kata Kunci")]
        elif temp_abs.find("Kata kunci") > -1:
            abstrak = temp_abs[temp_abs.find("Abstrak"):temp_abs.find("Kata kunci")]
        elif temp_abs.find("Keywords") > -1:
            abstrak = temp_abs[temp_abs.find("Abstrak"):temp_abs.find("Keywords")]
        
        if abstrak.find("Abstrak—") > -1: abstrak = abstrak.replace("Abstrak—", "")
        else: abstrak = abstrak.replace("Abstrak —", "")

        abstrak = abstrak.replace("\n", " ")
        isi = temp_isi[temp_isi.find("PENDAHULUAN"):temp_isi.find("REFERENSI")]
        isi = isi.replace("\n", " ")
        isi = isi.replace("PENDAHULUAN", "")
        isi = isi.replace("Gbr.", "")
        repl = re.compile(re.escape("CESS (Journal of Computer Engineering System and Science)"), re.IGNORECASE)
        isi = repl.sub('', isi)
        content = {
            'abstrak': abstrak,
            'isi': isi,
        }
        return content

    def get_keyword(self):
        temp_key = self.page_content[200:4000]
        if temp_key.find("Kata Kunci") > -1:
            keywords = temp_key[temp_key.find("Kata Kunci"):temp_key.find("I. PENDAHULUAN")]
            if keywords.find("Kata Kunci—") > -1: keywords = keywords.replace("Kata Kunci—", "")
            else: keywords = keywords.replace("Kata Kunci —", "")
        elif temp_key.find("Kata kunci") > -1:
            keywords = temp_key[temp_key.find("Kata kunci"):temp_key.find("I. PENDAHULUAN")]
            if keywords.find("Kata kunci—") > -1: keywords = keywords.replace("Kata kunci—", "")
            else: keywords = keywords.replace("Kata kunci —", "")
        elif temp_key.find("Keywords") > -1:
            keywords = temp_key[temp_key.find("Keywords"):temp_key.find("I. PENDAHULUAN")]
            if keywords.find("Keywords—") > -1: keywords = keywords.replace("Keywords—", "")
            else: keywords = keywords.replace("Keywords —", "")

        keywords = re.sub(r'[^a-z0-9 ,-]', ' ', keywords, flags = re.IGNORECASE|re.MULTILINE)

        return keywords

def handle_uploaded_file(f):
    with open('some/file/name.txt', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


# Word Tagging
class WordTagging:

    # Constructor
    def __init__(self):
        self.word_dict = {}
        self.word_color = {
            'Nomina': 'has-text-link',
            'Adjektiva': 'has-text-primary',
            'Verba': 'has-text-warning',
            'Verba-Definitif': 'has-text-warning',
            'Adverbia': 'has-text-info',
            'Preposisi': 'has-text-grey-light',
            'Interjeksi': 'has-text-info',
            'Konjungsi': 'has-text-grey-light',
            'Pronomina': 'has-text-success',
            'Lain-lain': 'has-text-danger',
            'Numeralia': 'has-text-info',
            'Separator': 'has-text-grey-light',
            'Titik': 'has-text-grey-light',
        }
        self.add_root_word()

    # Return word color by type
    def get_word_color(self):
        return self.word_color

    # Add base word & type dictionary
    def add_root_word(self):
        word_root = Katadasar.objects.values_list('katadasar', flat=True)
        word_type = Katadasar.objects.values_list('tipe_katadasar', flat=True)
        self.word_dict = dict(zip(word_root, word_type))

    # Identify type of word
    def identify_word(self, stemmed_word, origin_word ):
        word_type = {'tipe_katadasar': 'Lain-lain'}

        if stemmed_word in self.word_dict:

            if origin_word.lower() == 'adalah' or origin_word.lower() == 'merupakan':
                return 'Verba-Definitif'

            elif stemmed_word == origin_word.lower():
                return self.word_dict[stemmed_word]

            elif origin_word.lower().find('me') == 0 or origin_word.lower().find('di') == 0:
                return 'Verba'

            elif origin_word.lower().find('pe') == 0 and re.match(r'^(.*)(an|nya)$', origin_word.lower()) or origin_word.lower().find('ke') == 0:
                return 'Nomina'

            elif origin_word.lower().find('ter') == 0 or origin_word.lower().find('se') == 0:
                return 'Preposisi'

            elif origin_word.lower().find('ber') == 0:
                return 'Adjektiva'

            else:
                return self.word_dict[stemmed_word]

        elif origin_word.lower() == 'dll' or origin_word.lower() == 'dsb' or origin_word.lower() == 'etc':
            return 'Konjungsi'

        elif origin_word.lower() == ',':
            return 'Separator'
        
        elif origin_word.lower() == '.':
            return 'Titik'
        
        elif re.match(r'[0-9]', origin_word.lower()):
            return 'Numeralia'

        else:
            return word_type['tipe_katadasar']

    # Identify each word in sentence
    def identify_text(self, stemmed_sentence, origin_sentence):
        type_list = []
        for i, stemmed_word in enumerate(stemmed_sentence):
            type_temp = self.identify_word(stemmed_word, origin_sentence[i])
            type_list.append({'type': type_temp, 'color':self.word_color[type_temp]})
        return type_list


# Processing steps
class Processing:

    def __init__(self):
        self.word_phrase_list = {}
        self.word_phrase_list_array = []
        self.word_graph = nx.DiGraph()
        self.word_graph_undirect = nx.Graph()
        self.word_score_list = {}

    def get_word_phrase_list(self):
        return self.word_phrase_list

    def get_in_edges(self, keys):
        return [(key) for key in self.word_graph.predecessors(keys)]

    def get_out_edges(self, keys):
        return [(key) for key in self.word_graph.successors(keys)]

    def get_undirect_edges(self, keys):
        return [(key) for key in self.word_graph_undirect.neighbors(keys)]

    def get_word_score_list(self):
        sorted_score = sorted(self.word_score_list.items(), key=lambda kv: kv[1], reverse=True)
        sorted_score[10:len(sorted_score)] = []
        return sorted_score

    def get_all_edges(self, undirect=False):
        all_edges = {}
        if undirect:
            for key, value in self.word_phrase_list.items():
                all_edges.update({key: {
                    'neighbors': self.get_undirect_edges(key),
                }})
        else:
            for key, value in self.word_phrase_list.items():
                all_edges.update({key: {
                    'in': self.get_in_edges(key),
                    'out': self.get_out_edges(key),
                }})
        return all_edges

    def add_key_candidate(self, keys, key_type, sentence_id, lexical_index):
        if keys in self.word_phrase_list:
            has_sentence = self.word_phrase_list[keys]['has_sentence']
            has_sentence.append(sentence_id)
            temp_lexical_index =  self.word_phrase_list[keys]['lexical_index']
            temp_lexical_index.append(lexical_index)
            self.word_phrase_list.update({
                keys:{
                    'type':key_type, 
                    'has_sentence': has_sentence, 
                    'lexical_index': temp_lexical_index
                }
            })
        else:
            self.word_phrase_list.update({
                keys: {
                    'type':key_type, 
                    'has_sentence': [sentence_id], 
                    'lexical_index': [lexical_index]
                }
            })
        self.word_phrase_list_array.append(keys)

    def process_word(self, sentence, basic=False):
        skip_word = 0
        lexical_index = 0
        sentence_id = 0

        for i, word in enumerate(sentence):

            if skip_word == 0:

                if word['type'] == 'Nomina' or word['type'] == 'Lain-lain':
                    keysid1 = word['origin']

                    if i < len(sentence)-1:
                        if sentence[i+1]['type'] == 'Nomina' or sentence[i+1]['type'] == 'Lain-lain':
                            if word['type'] != 'Lain-lain' and sentence[i+1]['type'] == 'Lain-lain': continue
                            
                            keysid2 = sentence[i+1]['origin']
                            keys2 = keysid1 + ' ' + keysid2

                            if i < len(sentence)-2:
                                if sentence[i+2]['type'] == 'Nomina' or sentence[i+2]['type'] == 'Lain-lain':
                                    keysid3 = sentence[i+2]['origin']
                                    keys3 = keys2 + ' ' + keysid3
                                    self.add_key_candidate(keys3, 'Phrase', sentence_id, lexical_index)
                                    skip_word = 2

                                else:
                                    self.add_key_candidate(keys2, 'Phrase', sentence_id, lexical_index)
                                    skip_word = 1

                            else:
                                self.add_key_candidate(keys2, 'Phrase', sentence_id, lexical_index)
                                skip_word = 1

                        else:
                            self.add_key_candidate(keysid1, 'Word', sentence_id, lexical_index)
                    
                    else:
                        self.add_key_candidate(keysid1, 'Word', sentence_id, lexical_index)
                    
                elif word['type'] == 'Verba':
                    if not basic:
                        keys = word['origin']
                        self.add_key_candidate(keys, 'Verba', sentence_id, lexical_index)
                    lexical_index += 1

                elif word['type'] == 'Verba-Definitif':
                    if not basic:
                        keys = word['origin']
                        self.add_key_candidate(keys, 'Verba-Definitif', sentence_id, lexical_index)
                    lexical_index += 1

                elif word['type'] == 'Titik':
                    sentence_id += 1
                    lexical_index = 0

            else:
                skip_word-=1
                continue

        if basic:
            self.make_undirect_graph()
            self.make_undirect_edges()
            self.calc_ori_textrank()
        else:
            self.make_word_graph()
            self.make_graph_edges()
            self.calc_modif_textrank()

    def make_word_graph(self):
        self.word_graph.add_nodes_from(self.word_phrase_list)
    
    def make_undirect_graph(self):
        self.word_graph_undirect.add_nodes_from(self.word_phrase_list)

    def make_graph_edges(self):
        for key_i, value_i in self.word_phrase_list.items():
            if value_i['type'] == 'Verba' or value_i['type'] == 'Verba-Definitif':
                for k, sentence_id in enumerate(value_i['has_sentence']):
                    for key_j, value_j in self.word_phrase_list.items():
                        if sentence_id in value_j['has_sentence'] and (value_j['type'] == 'Phrase' or value_j['type'] == 'Word'):
                            self.word_graph.add_edge(key_i, key_j)

    def make_undirect_edges(self):
        for word_index in range(len(self.word_phrase_list_array)-2):
            self.word_graph_undirect.add_edge(
                self.word_phrase_list_array[word_index], 
                self.word_phrase_list_array[word_index+1]
            )

    def calc_modif_textrank(self, tr_iter=0):
        d_value = 0.85
        
        for key_i, value_i in self.word_phrase_list.items():
            if value_i['type'] == 'Phrase' or value_i['type'] == 'Word':
                score_j = 0

                for pred_i in self.get_in_edges(key_i):
                    weight_j = 1
                    
                    if self.word_phrase_list[pred_i]['type'] == 'Verba-Definitif':
                        weight_j += 2
                    if 0 in self.word_phrase_list[pred_i]['lexical_index']:
                        weight_j += 1
                    if 1 in self.word_phrase_list[pred_i]['lexical_index']:
                        weight_j += 0.5

                    score_j += weight_j / len(self.get_out_edges(pred_i)) 

                score_i = (1-d_value) + d_value * score_j
                self.word_score_list.update({key_i: score_i})

    def calc_ori_textrank(self, tr_iter=0):
        d_value = 0.85
        weight_before = 0.0
        
        if tr_iter > 0:
            weight_before = self.get_word_score_list()[0][1]

        for key_i, value_i in self.word_phrase_list.items():
            score_j = 0

            for neighbors_i in self.get_undirect_edges(key_i):
                try: weight_j = self.word_score_list[neighbors_i]
                except(KeyError): weight_j = 1
                score_j += weight_j / len(self.get_undirect_edges(neighbors_i)) 

            weight_i = (1-d_value) + d_value * score_j
            
            self.word_score_list.update({key_i: weight_i})
        
        if abs(self.get_word_score_list()[0][1] - weight_before) > 0.0001:
            self.calc_ori_textrank(tr_iter+1)
        

class Analyzing:

    def __init__(self, content, basic=False):
        self.stemmed_list = []
        self.origin_list = []
        self.word_type_list = []
        self.word_data = []
        
        self.sentence_list = content
        
        self.wt_obj = WordTagging()
        self.proc_obj = Processing()
        self.main_analyze(basic)

    def get_stemmed_list(self):
        return self.stemmed_list

    def get_origin_list(self):
        return self.origin_list

    def get_word_type_list(self):
        return self.word_type_list

    def get_word_data(self):
        return self.word_data

    def get_sentence_list(self):
        return self.sentence_list

    def make_word_dict(self):
        temp_word_data = []
        for j, word in enumerate(self.origin_list):
            temp_word_data.append({
                'origin': self.origin_list[j],
                'stem': self.stemmed_list[j],
                'type': self.word_type_list[j]['type'],
                'color': self.word_type_list[j]['color'],
            })
        return temp_word_data

    def main_analyze(self, basic=False):

        # Stemming & Original Word List
        self.stemmed_list = stemming(self.sentence_list)
        self.origin_list = Tokenize.word_token(self.sentence_list)

        # Identify Word type
        self.word_type_list = self.wt_obj.identify_text(self.stemmed_list, self.origin_list)

        # Word dictionary
        temp_word_data = self.make_word_dict()
        
        # find word & phrase candidate
        self.proc_obj.process_word(temp_word_data, basic)
        
        self.word_data = temp_word_data

# Testing
class Testing:

    def __init__(self, original_keyword, extracted_keyword):
        self.original_keyword = original_keyword.lower().split(', ')
        self.extracted_keyword = []
        self.precision = 0.0
        self.recall = 0.0
        self.f_measure = 0.0

        self.convert_to_array(extracted_keyword)
        self.calc_accuration()

    def calc_accuration(self):
        ori_ext = 0.0
        for ori in self.remove_ex_char(self.original_keyword):
            for ext in self.remove_ex_char(self.extracted_keyword):
                if ori == ext: ori_ext += 1
        self.precision = ori_ext / len(self.extracted_keyword)
        self.recall = ori_ext / len(self.original_keyword)
        if (self.precision + self.recall) > 0:
            self.f_measure = (2 * self.precision * self.recall) / (self.precision + self.recall)
        else:
            self.f_measure = 0

    def calc_new_accuracy(self):
        ori_ext = 0.0
        for ori in self.remove_ex_char(self.original_keyword):
            for ext in self.remove_ex_char(self.extracted_keyword):
                if ori == ext: ori_ext += 1
        self.precision = ori_ext / len(self.extracted_keyword)
        self.recall = ori_ext / len(self.original_keyword)
        if (self.precision + self.recall) > 0:
            self.f_measure = ori_ext / ((len(self.extracted_keyword) - ori_ext) + len(self.original_keyword))
        else:
            self.f_measure = 0

    def remove_ex_char(self, keyword):
        result = []
        for key in keyword:
            res = re.sub(r'[^a-z0-9]', '', key, flags = re.IGNORECASE|re.MULTILINE)
            result.append(res)
        return result

    def get_original_keyword(self):
        return self.original_keyword

    def get_extracted_keyword(self):
        return self.extracted_keyword

    def convert_to_array(self, tuple_data):
        for i in tuple_data:
            self.extracted_keyword.append(i[0])

        
        