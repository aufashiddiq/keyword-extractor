from django.urls import path

from . import views

app_name = 'keyextractor'
urlpatterns = [
    path('', views.home_view, name='home'),
    path('article_create/', views.article_create_view, name='article_create'),
    path('article_create_post/', views.article_create_post, name='article_create_post'),
    path('<int:article_id>/article_detail/', views.article_detail_view, name='article_detail'),
    path('<int:article_id>/article_delete/', views.article_delete, name='article_delete'),
    path('<int:article_id>/article_analyze/', views.article_analyze, name='article_analyze'),
    path('<int:article_id>/article_analyze_basic/', views.article_analyze_basic, name='article_analyze_basic'),
    path('<int:article_id>/article_update/', views.article_update_view, name='article_update'),
    path('<int:article_id>/article_update_post/', views.article_update_post, name='article_update_post'),
    path('<int:article_id>/reset_extraction/', views.reset_extraction, name='reset_extraction'),
    path('reset_all_extraction/', views.reset_all_extraction, name='reset_all_extraction'),
    path('statistics/', views.statistics, name='statistics'),
    path('import_pdf/', views.import_pdf, name='import_pdf'),
    path('extract_all_keyword/', views.extract_all_keyword, name='extract_all_keyword'),
    path('extract_all_keyword_basic/', views.extract_all_keyword_basic, name='extract_all_keyword_basic')
]