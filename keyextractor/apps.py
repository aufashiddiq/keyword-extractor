from django.apps import AppConfig


class KeyextractorConfig(AppConfig):
    name = 'keyextractor'
