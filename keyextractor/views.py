from django.shortcuts import render
from django.views import View, generic
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.core.paginator import Paginator
from django.core.files.uploadedfile import UploadedFile, TemporaryUploadedFile

import ast
import time

from .models import Article

from .controllers import Analyzing, Testing, ImportPdf

def home_view(request):
    # article_list = Article.objects.all().order_by('-id')
    article_list = Article.objects.all().order_by('id')
    paginator = Paginator(article_list, 10)

    page = request.GET.get('page')
    articles = paginator.get_page(page)
    for article in articles:
        article.content = ast.literal_eval(article.content)

    context = {'article_list': articles, 'paginator': paginator}
    return render(request, 'keyextractor/article.html', context)

def article_create_view(request):
    return render(request, 'keyextractor/article_create.html', {
        'type': 'Simpan',
        'action': 'keyextractor:article_create_post',
        'id': ''
    })

def import_pdf(request):
    if request.method == 'POST':
        pdf_file = UploadedFile(file=request.FILES['file'])
        imp_obj = ImportPdf(pdf_file.name)

    article = {
        'title': imp_obj.get_title(),
        'content': imp_obj.get_content(),
        'original_keyword': imp_obj.get_keyword(),
    }
    return render(request, 'keyextractor/article_create.html', {
        'type': 'Simpan',
        'action': 'keyextractor:article_create_post',
        'id': '',
        'article': article
    })

def article_create_post(request):
    try:
        title = request.POST['title']
        content = request.POST['content']
        original_keyword = request.POST['original_keyword']
    except (KeyError):
        return render(request, 'keyextractor/article_create.html', {
            'error_message': "Failed to create article",
        })
    else:
        article = Article(title=title, content=content, original_keyword=original_keyword)
        article.save()
        return HttpResponseRedirect(reverse('keyextractor:home'))

def article_detail_view(request, article_id):
    try:
        article = Article.objects.get(pk=article_id)
        article.content = ast.literal_eval(article.content)
        2
    except(Article.DoesNotExist):
        return render(request, 'keyextractor/home.html', {
            'error_message': "Failed to load article",
        })
    else:
        if article.extracted_keyword:
            article.extracted_keyword = ast.literal_eval(article.extracted_keyword)
            article.accuracy = ast.literal_eval(article.accuracy)
        if article.extracted_keyword_ori:
            article.extracted_keyword_ori = ast.literal_eval(article.extracted_keyword_ori)
            article.accuracy_ori = ast.literal_eval(article.accuracy_ori)
        return render(request,'keyextractor/article_analyze.html', {'article': article})
        

def article_update_view(request, article_id):
    try:
        article = Article.objects.get(pk=article_id)
    except(Article.DoesNotExist):
        return render(request, 'keyextractor/home.html', {
            'error_message': "Failed to load article",
        })
    else:
        return render(request, 'keyextractor/article_create.html', {
            'type': 'Update',
            'action': 'keyextractor:article_update_post',
            'article': article,
            'id': article_id
        })

def article_update_post(request, article_id):
    try:
        title = request.POST['title']
        content = request.POST['content']
        original_keyword = request.POST['original_keyword']
        article = Article.objects.get(pk=article_id)
    except (KeyError, Article.DoesNotExist):
        return render(request, 'keyextractor/article_create.html', {
            'error_message': "Failed to create article",
        })
    else:
        article.title = title
        article.content = content
        article.original_keyword = original_keyword
        article.save()
        return HttpResponseRedirect(reverse('keyextractor:article_detail', args=[article_id]))

def article_delete(request, article_id):
    try:
        article = Article.objects.get(pk=article_id)
    except (Article.DoesNotExist):
        return render(request, 'keyextractor/home.html', {
            'error_message': "Delete Failed.",
        })
    else:
        article.delete()
        return HttpResponseRedirect(reverse('keyextractor:home'))

def article_analyze(request, article_id):
    start = int(round(time.time() * 1000))
    try:
        article = Article.objects.get(pk=article_id)
        article.content = ast.literal_eval(article.content)
        analyze_obj = Analyzing(article.content['abstrak']+article.content['isi'])
        testing_obj = Testing(article.original_keyword, analyze_obj.proc_obj.get_word_score_list())

    except (Article.DoesNotExist):
        return render(request, 'keyextractor/article_analyze.html', {
            'error_message': "Load Failed.",
        })
    else:
        end = int(round(time.time() * 1000))
        load_time = end - start

        if article.extracted_keyword_ori:
            article.extracted_keyword_ori = ast.literal_eval(article.extracted_keyword_ori)
            article.accuracy_ori = ast.literal_eval(article.accuracy_ori)
        article.extracted_keyword = analyze_obj.proc_obj.get_word_score_list()
        article.accuracy = {
            'f_measure': testing_obj.f_measure,
            'precision': testing_obj.precision,
            'recall': testing_obj.recall
        }
        article.load_time = load_time
        article.save()

        return render(request,'keyextractor/article_analyze.html', {
            'article': article, 
            'word_data': analyze_obj.get_word_data(),
            'color_dict': analyze_obj.wt_obj.get_word_color(),
            'word_phrase_list': analyze_obj.proc_obj.get_word_phrase_list(),
            'all_edges': analyze_obj.proc_obj.get_all_edges(),
            'testing_obj': testing_obj,
        })

def article_analyze_basic(request, article_id):
    start = int(round(time.time() * 1000))
    try:
        article = Article.objects.get(pk=article_id)
        article.content = ast.literal_eval(article.content)
        analyze_obj = Analyzing(article.content['abstrak']+article.content['isi'], True)
        testing_obj = Testing(article.original_keyword, analyze_obj.proc_obj.get_word_score_list())

    except (Article.DoesNotExist):
        return render(request, 'keyextractor/article_analyze.html', {
            'error_message': "Load Failed.",
        })
    else:
        end = int(round(time.time() * 1000))
        load_time = end - start

        if article.extracted_keyword:
            article.extracted_keyword = ast.literal_eval(article.extracted_keyword)
            article.accuracy = ast.literal_eval(article.accuracy)
        article.extracted_keyword_ori = analyze_obj.proc_obj.get_word_score_list()
        article.accuracy_ori = {
            'f_measure': testing_obj.f_measure,
            'precision': testing_obj.precision,
            'recall': testing_obj.recall
        }
        article.load_time_ori = load_time
        article.save()

        return render(request,'keyextractor/article_analyze.html', {
            'article': article, 
            'word_data': analyze_obj.get_word_data(),
            'color_dict': analyze_obj.wt_obj.get_word_color(),
            'word_phrase_list': analyze_obj.proc_obj.get_word_phrase_list(),
            'all_edges': analyze_obj.proc_obj.get_all_edges(True),
            'basic': True
        })

def reset_extraction(request, article_id):
    try:
        article = Article.objects.get(pk=article_id)
    except (KeyError, Article.DoesNotExist):
        return render(request, 'keyextractor/article_create.html', {
            'error_message': "Failed to get article",
        })
    else:
        article.extracted_keyword = None
        article.accuracy = None
        article.load_time = None
        article.extracted_keyword_ori = None
        article.accuracy_ori = None
        article.load_time_ori = None
        article.save()
        return HttpResponseRedirect(reverse('keyextractor:article_detail', args=[article_id]))

def statistics(request):
    article_list = Article.objects.all()

    sum_accuracy = 0.0
    extraction_time = 0
    data_length = 0
    best_accuracy = 0.0

    sum_accuracy_ori = 0.0
    extraction_time_basic = 0
    data_length_basic = 0
    best_accuracy_ori = 0.0

    for i, key in enumerate(article_list):
        if key.extracted_keyword:
            key.accuracy = ast.literal_eval(key.accuracy)
            sum_accuracy += float(key.accuracy['f_measure'])
            extraction_time += int(key.load_time)
            if float(key.accuracy['f_measure']) > best_accuracy: best_accuracy = float(key.accuracy['f_measure'])
            data_length += 1
        if key.extracted_keyword_ori:
            key.accuracy_ori = ast.literal_eval(key.accuracy_ori)
            sum_accuracy_ori += float(key.accuracy_ori['f_measure'])
            extraction_time_basic += int(key.load_time_ori)
            if float(key.accuracy_ori['f_measure']) > best_accuracy_ori: best_accuracy_ori = float(key.accuracy_ori['f_measure'])
            data_length_basic += 1
    if data_length == 0:
        average_time = 0
        average_acc = 0
    else:
        average_time = extraction_time/data_length
        average_acc = sum_accuracy/data_length

    if data_length_basic == 0:
        average_time_basic = 0
        average_acc_basic = 0
    else:
        average_time_basic = extraction_time_basic/data_length_basic
        average_acc_basic = sum_accuracy_ori/data_length_basic

    context = {
        'data': article_list, 
        'average_acc': average_acc, 
        'extraction_time': extraction_time,
        'average_time': average_time,
        'best_accuracy': best_accuracy,
        'average_acc_basic': average_acc_basic, 
        'extraction_time_basic': extraction_time_basic,
        'average_time_basic': average_time_basic,
        'best_accuracy_ori': best_accuracy_ori
    }
    return render(request, 'keyextractor/statistics.html', context)

def reset_all_extraction(request):
    try:
        data = Article.objects.all()
    except (KeyError, Article.DoesNotExist):
        return render(request, 'keyextractor/article.html', {
            'error_message': "Failed to get article",
        })
    else:
        for article in data:
            if article.extracted_keyword:
                article.extracted_keyword = None
                article.accuracy = None
                article.load_time = None
                article.extracted_keyword_ori = None
                article.accuracy_ori = None
                article.load_time_ori = None
                article.save()
        return HttpResponseRedirect(reverse('keyextractor:home'))

def extract_all_keyword(request):
    try:
        data = Article.objects.all()
    except (KeyError, Article.DoesNotExist):
        return render(request, 'keyextractor/article.html', {
            'error_message': "Failed to get article",
        })
    else:
        for article in data:
            start = int(round(time.time() * 1000))
            analyze_obj = Analyzing(article.content)
            testing_obj = Testing(article.original_keyword, analyze_obj.proc_obj.get_word_score_list())
            end = int(round(time.time() * 1000))
            load_time = end - start
            article.extracted_keyword = analyze_obj.proc_obj.get_word_score_list()
            article.accuracy = {
                'f_measure': testing_obj.f_measure,
                'precision': testing_obj.precision,
                'recall': testing_obj.recall
            }
            article.load_time = load_time
            article.save()

        return HttpResponseRedirect(reverse('keyextractor:statistics'))

def extract_all_keyword_basic(request):
    try:
        data = Article.objects.all()
    except (KeyError, Article.DoesNotExist):
        return render(request, 'keyextractor/article.html', {
            'error_message': "Failed to get article",
        })
    else:
        for article in data:
            # if article.id != 6 and article.id != 11:
            start = int(round(time.time() * 1000))
            analyze_obj = Analyzing(article.content, True)
            testing_obj = Testing(article.original_keyword, analyze_obj.proc_obj.get_word_score_list())
            end = int(round(time.time() * 1000))
            load_time = end - start
            article.extracted_keyword_ori = analyze_obj.proc_obj.get_word_score_list()
            article.accuracy_ori = {
                'f_measure': testing_obj.f_measure,
                'precision': testing_obj.precision,
                'recall': testing_obj.recall
            }
            article.load_time_ori = load_time
            article.save()

        return HttpResponseRedirect(reverse('keyextractor:statistics'))